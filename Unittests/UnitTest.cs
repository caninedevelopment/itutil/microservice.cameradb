using NUnit.Framework;
using Monosoft.Service.CameraDB.v1;

namespace Unittests
{
    [TestFixture]
    public class CameraDB
    {
        Logic logic = new Logic();


        [Test]
        public void dooo()
        {
            var d = logic.GetCameraCapacity("AXIS%20M3025");
            var j = Newtonsoft.Json.JsonConvert.SerializeObject(d);
            Assert.IsTrue(true);


        }
        //[Test]
        //public void Test_Scan_Network()
        //{
        //    var filedata = System.IO.File.ReadAllText("lastscan.json");
        //    logic.ScanNetworkForCameras(filedata);
        //}

        [Test]
        public void Test_GetAllCameras()
        {
            var all = logic.GetAllCameras();
            Assert.IsTrue(all.cameraInfos.Count > 0);
        }

        [Test]
        public void Test_UpdateACamera()
        {
            var all = logic.GetAllCameras();

            var first = all.cameraInfos[0];

            first.name = "Test";
            logic.UpdateCamera(first);

            all = logic.GetAllCameras();


            Assert.AreEqual("Test", all.cameraInfos[0].name);
        }

        //[Test]
        //public void CameraDBTest()
        //{
        //    bool t = false;
        //    string output = "";
        //    Camera cam = new Camera();
        //    cam.location = new Location();
        //    cam.location.name = "Grenaa";
        //    cam.location.latitude = 56.411996;
        //    cam.location.longitude = 10.893631;
        //    cam.brand = "SpyCam";
        //    cam.IP = "192.168.121.123";

        //    Database db = new Database();

        //    db.Create(cam);
        //    AllCameras AC = logic.GetAllCameras();

        //    if(AC.cameraInfos[0].brand == "SpyCam")
        //    {
        //        output += "Create = ok, GetAll = ok, ";
        //    }

        //    CameraInfo info = AC.cameraInfos[0];
        //    info.brand = "xDCam";
        //    db.Update(info);

        //    CameraRequest camreq = new CameraRequest();
        //    camreq.id = AC.cameraInfos[0].id;

        //    Camera CI = logic.GetCamera(camreq);

        //    if (CI.brand == "xDCam")
        //    {
        //        output += "Update = ok, GetFromID = ok, ";
        //    }

        //    db.Remove(camreq);

        //    AC = logic.GetAllCameras();

        //    if(AC.cameraInfos.Count <= 0)
        //    {
        //        output += "Remove = ok";
        //    }

        //    if(output == "Create = ok, GetAll = ok, Update = ok, GetFromID = ok, Remove = ok")
        //    {
        //        t = true;
        //    }

        //    db.DeleteDatabase();

        //    Assert.IsTrue(t, "CameraDB test true");
        //}
    }
}