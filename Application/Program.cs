﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application.CameraDB
{
    class Program
    {
        static void Main(string[] args)
        {
            ITUtil.Common.Console.Program.StartRabbitMq(new System.Collections.Generic.List<ITUtil.Common.Command.INamespace>()
            {
                new V1.Dto.CameraDb.Namespace(),
                new V1.Dto.CameraModel.Namespace(),
            });
        }
    }
}
