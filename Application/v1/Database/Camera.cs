﻿using Monosoft.Service.CameraDB.V1.DTO;
using System;

namespace Monosoft.Service.CameraDB.v1.Database
{
    public class Camera
    {
        public Guid Id { get; set; }
        public CameraInfo CameraInfo { get; set; }
        public string IPAddress { get; set; }
        public DateTime lastUpdated { get; set; }
    }
}
