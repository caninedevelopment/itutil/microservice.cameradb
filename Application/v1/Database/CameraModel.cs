﻿using Monosoft.Service.CameraDB.V1.DTO;

namespace Monosoft.Service.CameraDB.v1.Database
{
    class CameraModel
    {
        public string Name { get; set; }
        public CameraModelCapacity definition { get; set; }
    }
}
