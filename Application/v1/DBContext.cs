using System;
using System.Collections.Generic;
using System.Linq;
using ServiceStack.OrmLite;
using Monosoft.Service.CameraDB.V1.DTO;
using ITUtil.Common.Config;

namespace Monosoft.Service.CameraDB.v1
{
    public class DBContext
    {
        private static OrmLiteConnectionFactory dbFactory = null;
        private static string dbName = "cameradatabase";

        private static DBContext _instance = null;
        public static DBContext Instance
        {
            get
            {
                if (_instance == null)
                {
                    var settings = ITUtil.Common.Config.GlobalRessources.getConfig().GetSetting<SQLSettings>();
                    _instance = new DBContext(settings);
                }
                return _instance;
            }
        }

        private DBContext(SQLSettings settings)
        {
            OrmLiteConnectionFactory masterDb = null;
            masterDb = GetConnectionFactory(settings);

            using (var db = masterDb.Open())
            {
                settings.CreateDatabaseIfNoExists(db, dbName);
            }

            // set the factory up to use the microservice database
            dbFactory = GetConnectionFactory(settings, dbName);
            using (var db = dbFactory.Open())
            {
                db.CreateTableIfNotExists<v1.Database.Camera>();
                db.CreateTableIfNotExists<v1.Database.CameraModel>();

                var res = db.SingleById<v1.Database.CameraModel>("AXIS%20M3025");
                if (res == null)
                {
                    db.Insert<v1.Database.CameraModel>(new Database.CameraModel()
                    {
                        Name = "AXIS%20M3025",
                        definition = new CameraModelCapacity()
                        {
                            protocols = new List<RequestStreamUri.protocoltype>() { RequestStreamUri.protocoltype.rtsp, RequestStreamUri.protocoltype.http },
                            supportedResolutions = new List<Resolution>()
                              {
                                  new Resolution() { supportedFPS=new List<int> { 1,5,15,20,30}, x=1920, y=1080 },
                                  new Resolution() { supportedFPS=new List<int> { 1,5,15,20,30}, x=1280, y=960 },
                                  new Resolution() { supportedFPS=new List<int> { 1,5,15,20,30}, x=1024, y=768 },
                                  new Resolution() { supportedFPS=new List<int> { 1,5,15,20,30}, x=800, y=600 },
                                  new Resolution() { supportedFPS=new List<int> { 1,5,15,20,30}, x=640, y=480 },
                                  new Resolution() { supportedFPS=new List<int> { 1,5,15,20,30}, x=480, y=360 },
                                  new Resolution() { supportedFPS=new List<int> { 1,5,15,20,30}, x=320, y=240 },
                                  new Resolution() { supportedFPS=new List<int> { 1,5,15,20,30}, x=1280, y=1025 },
                                  new Resolution() { supportedFPS=new List<int> { 1,5,15,20,30}, x=1280, y=720 },
                                  new Resolution() { supportedFPS=new List<int> { 1,5,15,20,30}, x=640, y=360 },
                                  new Resolution() { supportedFPS=new List<int> { 1,5,15,20,30}, x=352, y=240 },
                                },
                            supportedCompression = new List<int> { 5, 10, 15, 20, 25, 30, 35, 40, 45, 55, 65, 70, 75, 80, 85, 90, 95 }
                        }
                    });
                }
            }

        }
        private static OrmLiteConnectionFactory GetConnectionFactory(SQLSettings settings, string databasename = "")
        {
            switch (settings.ServerType)
            {
                case SQLSettings.SQLType.MSSQL:
                    return new OrmLiteConnectionFactory(settings.ConnectionString(databasename), SqlServerDialect.Provider);
                case SQLSettings.SQLType.POSTGRESQL:
                    return new OrmLiteConnectionFactory(settings.ConnectionString(databasename), PostgreSqlDialect.Provider);
                case SQLSettings.SQLType.MYSQL:
                    return new OrmLiteConnectionFactory(settings.ConnectionString(databasename), MySqlDialect.Provider);
                default:
                    return null;
            }
        }

        public void Cleanup()
        {
            using (var db = dbFactory.Open())
            {
                var dbresult = db.DeleteAll<v1.Database.Camera>();
            }
        }


        public bool UpdateModel(CameraModelCapacity updatedInfo)
        {
            using (var db = dbFactory.Open())
            {
                var dbresult = db.SingleById<v1.Database.CameraModel>(updatedInfo.name);

                if (dbresult != null)
                {
                    db.Update<v1.Database.CameraModel>(new Database.CameraModel() { Name=updatedInfo.name, definition = updatedInfo });
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public void RemoveModel(CameraModelName updatedInfo)
        {
            using (var db = dbFactory.Open())
            {
                db.Delete<v1.Database.CameraModel>(p => p.Name == updatedInfo.Name);
            }
        }
        public CameraModelCapacity GetModel(string name)
        {
            using (var db = dbFactory.Open())
            {
                var res = db.SingleById<v1.Database.CameraModel>(name);
                if (res != null)
                {
                    return res.definition;
                } else
                {
                    return null;
                }
            }
        }
        public List<CameraModelCapacity> GetAllModels()
        {
            using (var db = dbFactory.Open())
            {
                return db.Select<v1.Database.CameraModel>().Select(p => p.definition).ToList(); 
            }
        }

        public CameraInfo GetFromIP(string ip)
        {
            using (var db = dbFactory.Open())
            {
                return db.Select<v1.Database.Camera>(p => p.IPAddress == ip).Select(p=>p.CameraInfo).FirstOrDefault();
            }
        }

        public void CreateOrUpdate(CameraInfo input)
        {
            input.id = Guid.NewGuid();
            using (var db = dbFactory.Open())
            {
                var res = db.Select<v1.Database.Camera>(p => p.IPAddress == input.IP).FirstOrDefault();
                if (res != null)
                {
                    res.CameraInfo = input;
                    db.Update<v1.Database.Camera>(res);
                }
                else
                {

                    db.Insert<v1.Database.Camera>(new Database.Camera()
                    {
                        CameraInfo = input,
                        Id = input.id,
                        IPAddress = input.IP,
                        lastUpdated = DateTime.Now
                    });
                }
            }
        }
        public bool Update(CameraInfo updatedInfo)
        {
            using (var db = dbFactory.Open())
            {
                var res = db.Select<v1.Database.Camera>(p => p.Id == updatedInfo.id).FirstOrDefault();
                if (res != null)
                {
                    //? res.IPAddress = updatedInfo.IP;
                    res.CameraInfo = updatedInfo;
                    db.Update<v1.Database.Camera>(res);
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public CameraInfo GetFromId(Guid id)
        {
            using (var db = dbFactory.Open())
            {
                return db.Select<v1.Database.Camera>(p => p.Id == id).Select(p=>p.CameraInfo).FirstOrDefault();
            }
        }
        public bool Remove(CameraRequest removerequest)
        {
            using (var db = dbFactory.Open())
            {
                db.Delete<v1.Database.Camera>(p => p.Id == removerequest.id);
                return true;
            }
        }
        public List<CameraInfo> ListAll()
        {
            using (var db = dbFactory.Open())
            {
                return db.Select<v1.Database.Camera>().Select(p => p.CameraInfo).ToList();
            }

        }
    }
}
