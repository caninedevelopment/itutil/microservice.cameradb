namespace Monosoft.Service.CameraDB.v1
{
    using ITUtil.Common.RabbitMQ.DTO;
    using Monosoft.Service.CameraDB.V1.DTO;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;

    public class Logic
    {
        public Logic()
        {
        }

        public void ScanNetworkForCameras(string json = null)
        {
            try
            {
                ProcessStartInfo psi = new ProcessStartInfo();
                psi.FileName = "ScanConsole.exe";
                psi.WorkingDirectory = System.IO.Directory.GetCurrentDirectory();
                psi.UseShellExecute = true;
                var process = System.Diagnostics.Process.Start(psi);
                process.WaitForExit();

            }
            finally
            {

                json = System.IO.File.ReadAllText("lastscan.json");

                var _deviceList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<DiscoveredDeviceInfo>>(json);

                foreach (var d in _deviceList)
                {
                    var caminfo = new CameraInfo()
                    {
                        model = d.Name,
                        name = "",
                        IP = d.Host,
                    };
                    DBContext.Instance.CreateOrUpdate(caminfo);
                }
            }
        }

        public void DeleteCamera(CameraRequest input)
        {
            DBContext.Instance.Remove(input);
        }
        public CameraInfo UpdateCamera(CameraInfo input)
        {
            if (DBContext.Instance.Update(input))
            {
                return input;
            }
            else
            {
                return null;
            }
        }

        public void DeleteCameraModel(CameraModelName input)
        {
            DBContext.Instance.RemoveModel(input);
        }
        public CameraModelCapacity UpdateCameraModel(CameraModelCapacity input)
        {
            if (DBContext.Instance.UpdateModel(input))
            {
                return input;
            }
            else
            {
                return null;
            }
        }

        public CameraModelCapacity GetCameraCapacity(string model)
        {
            return DBContext.Instance.GetModel(model);
        }
        public List<CameraModelCapacity> GetAllCameraCapacity()
        {
            return DBContext.Instance.GetAllModels();
        }

        public ResponseStreamUri GetCameraStream(RequestStreamUri input)
        {
            var camera = DBContext.Instance.GetFromId(input.id);
            //TODO: based on camera.model -> find best matching URI with valid resolution, fps and compression....
            var capacity = GetCameraCapacity(camera.model);
            if (capacity == null)
            {
                throw new Exception("Unknown cameramodel");
            }
            else
            {
                var match = capacity.supportedResolutions.Where(p => p.x >= input.resolutionX && p.y >= input.resolutionY).OrderBy(p => p.x).ThenBy(p => p.y).FirstOrDefault();
                if (match == null)
                {
                    match = capacity.supportedResolutions.Where(p => p.x <= input.resolutionX && p.y <= input.resolutionY).OrderByDescending(p => p.x).ThenByDescending(p => p.y).FirstOrDefault();
                }

                var fps = match.supportedFPS.Where(p => p >= input.fps).OrderBy(p => p).FirstOrDefault();
                if (fps == 0)
                {
                    fps = match.supportedFPS.Where(p => p <= input.fps).OrderByDescending(p => p).FirstOrDefault();
                }

                var compression = capacity.supportedCompression.Where(p => p >= input.compression).OrderBy(p => p).FirstOrDefault();
                if (compression == 0)
                {
                    compression = capacity.supportedCompression.Where(p => p <= input.compression).OrderByDescending(p => p).FirstOrDefault();
                }


                if (camera.model.ToLower().Contains("axis")) //edit url
                {
                    switch (input.protocol)
                    {
                        case RequestStreamUri.protocoltype.http:
                            if (capacity.protocols.Contains(RequestStreamUri.protocoltype.http))
                            {
                                return new ResponseStreamUri() { uri = $"http://{camera.IP}/mjpg/video.mjpg?resolution={match.x}x{match.y}&compression={compression}&fps={fps}", username = camera.username, password = camera.password };
                            }
                            break;
                        case RequestStreamUri.protocoltype.https:
                            if (capacity.protocols.Contains(RequestStreamUri.protocoltype.https))
                            {
                                return new ResponseStreamUri() { uri = $"https://{camera.IP}/mjpg/video.mjpg?resolution={match.x}x{match.y}&compression={compression}&fps={fps}", username = camera.username, password = camera.password };
                            }
                            break;
                        case RequestStreamUri.protocoltype.rtp:
                            if (capacity.protocols.Contains(RequestStreamUri.protocoltype.rtp))
                            {
                                return new ResponseStreamUri() { uri = $"rtp://{camera.IP}/axis-media/media.amp?resolution={match.x}x{match.y}&compression={compression}&fps={fps}", username = camera.username, password = camera.password };
                            }
                            break;
                        case RequestStreamUri.protocoltype.rtsp:
                            if (capacity.protocols.Contains(RequestStreamUri.protocoltype.rtsp))
                            {
                                return new ResponseStreamUri() { uri = $"rtsp://{camera.IP}:554/axis-media/media.amp?resolution={match.x}x{match.y}&compression={compression}&fps={fps}", username = camera.username, password = camera.password };
                            }
                            break;
                        default:
                            break;
                    }
                    throw new System.Exception("Unkown protocol for axis");
                }
                else
                {
                    throw new System.Exception("Unkown camera brand");
                }
            }
        }

        public CameraInfo GetCamera(CameraRequest input)
        {
            return DBContext.Instance.GetFromId(input.id);
        }
        public AllCameras GetAllCameras()
        {
            AllCameras all = new AllCameras();
            all.cameraInfos = DBContext.Instance.ListAll();
            return all;
        }
    }
}