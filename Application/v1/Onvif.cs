﻿using System;
using System.Collections.Generic;

namespace Monosoft.Service.CameraDB.v1
{
    public class DiscoveredDeviceInfo
    {
        public string Host { get; set; }
        public int Port { get; set; }
        public Uri Uri { get; set; }
        public string Profile { get; set; }
        public string Model { get; set; }
        public string Name { get; set; }
        public string Location { get; set; }
    }
}