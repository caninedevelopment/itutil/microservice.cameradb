//namespace Monosoft.Service.CameraDB.v1
//{
//    using System.Collections.Generic;
//    using Monosoft.Service.CameraDB.v1.DTO;
//    using System;
//    using ITUtil.Common.DTO;
//    using ITUtil.Common.Utils;
//    using ITUtil.Common.Utils.Bootstrapper;
//    using static ITUtil.Common.Utils.Bootstrapper.OperationDescription;

//    public class Controller_CameraDb : IMicroservice
//    {
//        private static Logic logic = new Logic();


//        public string ServiceName
//        {
//            get { return "CameraDB"; }
//        }

//        public ProgramVersion ProgramVersion { get { return new ProgramVersion("1.0.0.0"); } }

//        public static MetaDataDefinition admin = new MetaDataDefinition("Monosoft.Service.CameraDb", "admin", MetaDataDefinition.DataContextEnum.organisationClaims, new LocalizedString("EN", "User is administrator"));
//        public static MetaDataDefinition camoperator = new MetaDataDefinition("Monosoft.Service.CameraDb", "Operator", MetaDataDefinition.DataContextEnum.organisationClaims, new LocalizedString("EN", "User is an camera operator"));
//        public List<OperationDescription> OperationDescription
//        {
//            get
//            {
//                return new List<OperationDescription>()
//                {
//                    new OperationDescription(
//                        "update",
//                        "Updates a camera in the database",
//                        typeof(CameraInfo),
//                        typeof(CameraInfo),
//                        new MetaDataDefinitions(new MetaDataDefinition[] { admin }),
//                        UpdateCamera
//                    ),
//                    new OperationDescription(
//                        "delete",
//                        "Removes a cemera from database",
//                        typeof(CameraRequest),
//                        null,
//                        new MetaDataDefinitions(new MetaDataDefinition[] { admin }),
//                        DeleteCamera
//                    ),
//                    new OperationDescription(
//                        "scan",
//                        "Scans the network for camera and adds them to database",
//                        null,
//                        null,
//                        new MetaDataDefinitions(new MetaDataDefinition[] { admin }),
//                        ScanForCameras
//                    ),
//                    new OperationDescription(
//                        "getCamera",
//                        "Returns the information of the chosen camera",
//                        typeof(CameraRequest),
//                        typeof(CameraInfo),
//                        new MetaDataDefinitions(new MetaDataDefinition[] { admin }),
//                        GetCameraInfo
//                    ),
//                    new OperationDescription(
//                        "getCameraStream",
//                        "Returns the stream uri for a camera",
//                        typeof(RequestStreamUri),
//                        typeof(ResponseStreamUri),
//                        new MetaDataDefinitions(new MetaDataDefinition[] { admin, camoperator }),
//                        GetCameraStream
//                    ),

//                    new OperationDescription(
//                        "getAllCamera",
//                        "Returns a list of all cameras in the database",
//                        null,
//                        typeof(AllCameras),
//                        new MetaDataDefinitions(new MetaDataDefinition[] { admin }),
//                        GetAllCameras
//                    )
//                };
//            }
//        }

//        public static ReturnMessageWrapper UpdateCamera(MessageWrapper wrapper)
//        {
//            var input = MessageDataHelper.FromMessageData<CameraInfo>(wrapper.messageData);
//            var res = logic.UpdateCamera(input);
//            return ReturnMessageWrapper.CreateResult(true, wrapper, res, LocalizedString.OK);
//        }
//        public static ReturnMessageWrapper ScanForCameras(MessageWrapper wrapper)
//        {
//            logic.ScanNetworkForCameras();
//            return ReturnMessageWrapper.CreateResult(true, wrapper, null, LocalizedString.OK);
//        }
//        public static ReturnMessageWrapper DeleteCamera(MessageWrapper wrapper)
//        {
//            var input = MessageDataHelper.FromMessageData<CameraRequest>(wrapper.messageData);
//            logic.DeleteCamera(input);
//            return ReturnMessageWrapper.CreateResult(true, wrapper, null, LocalizedString.OK);
//        }

        
//        public static ReturnMessageWrapper GetCameraStream(MessageWrapper wrapper)
//        {
//            var input = MessageDataHelper.FromMessageData<RequestStreamUri>(wrapper.messageData);
//            ResponseStreamUri caminfo = logic.GetCameraStream(input);
//            return ReturnMessageWrapper.CreateResult(true, wrapper, caminfo, LocalizedString.OK);
//        }

//        public static ReturnMessageWrapper GetCameraInfo(MessageWrapper wrapper)
//        {
//            var input = MessageDataHelper.FromMessageData<CameraRequest>(wrapper.messageData);
//            CameraInfo caminfo = logic.GetCamera(input);

//            return ReturnMessageWrapper.CreateResult(true, wrapper, caminfo, LocalizedString.OK);
//        }
//        public static ReturnMessageWrapper GetAllCameras(MessageWrapper wrapper)
//        {
//            var input = MessageDataHelper.FromMessageData<CameraRequest>(wrapper.messageData);
//            AllCameras info = logic.GetAllCameras();

//            return ReturnMessageWrapper.CreateResult(true, wrapper, info, LocalizedString.OK);
//        }
//    }
//}
