//namespace Monosoft.Service.CameraDB.v1
//{
//    using System.Collections.Generic;
//    using Monosoft.Service.CameraDB.v1.DTO;
//    using System;
//    using ITUtil.Common.DTO;
//    using ITUtil.Common.Utils;
//    using ITUtil.Common.Utils.Bootstrapper;
//    using static ITUtil.Common.Utils.Bootstrapper.OperationDescription;

//    public class Controller_CameraModel: IMicroservice
//    {
//        private static Logic logic = new Logic();


//        public string ServiceName
//        {
//            get { return "CameraModel"; }
//        }

//        public ProgramVersion ProgramVersion { get { return new ProgramVersion("1.0.0.0"); } }

//        public static MetaDataDefinition admin = new MetaDataDefinition("Monosoft.Service.CameraModel", "admin", MetaDataDefinition.DataContextEnum.organisationClaims, new LocalizedString("EN", "User is administrator"));
//        public List<OperationDescription> OperationDescription
//        {
//            get
//            {
//                return new List<OperationDescription>()
//                {
//                    new OperationDescription(
//                        "update",
//                        "Updates the model definition",
//                        typeof(CameraModelCapacity),
//                        typeof(CameraModelCapacity),
//                        new MetaDataDefinitions(new MetaDataDefinition[] { admin }),
//                        UpdateCamera
//                    ),
//                    new OperationDescription(
//                        "delete",
//                        "Removes a model definition from database",
//                        typeof(CameraModelName),
//                        null,
//                        new MetaDataDefinitions(new MetaDataDefinition[] { admin }),
//                        DeleteCamera
//                    ),
//                    new OperationDescription(
//                        "get",
//                        "Gets the model definition",
//                        typeof(CameraModelName),
//                        typeof(CameraModelCapacity),
//                        new MetaDataDefinitions(new MetaDataDefinition[] { admin }),
//                        GetCameraModel
//                    ),
//                    new OperationDescription(
//                        "getAll",
//                        "Gets the model definition",
//                        null,
//                        typeof(List<CameraModelCapacity>),
//                        new MetaDataDefinitions(new MetaDataDefinition[] { admin }),
//                        GetAllCameraModel
//                    ),
//                };
//            }
//        }

//        public static ReturnMessageWrapper UpdateCamera(MessageWrapper wrapper)
//        {
//            var input = MessageDataHelper.FromMessageData<CameraModelCapacity>(wrapper.messageData);
//            var res = logic.UpdateCameraModel(input);
//            return ReturnMessageWrapper.CreateResult(true, wrapper, input, LocalizedString.OK);
//        }
//        public static ReturnMessageWrapper DeleteCamera(MessageWrapper wrapper)
//        {
//            var input = MessageDataHelper.FromMessageData<CameraModelName>(wrapper.messageData);
//            logic.DeleteCameraModel(input);
//            return ReturnMessageWrapper.CreateResult(true, wrapper, null, LocalizedString.OK);
//        }
//        public static ReturnMessageWrapper GetCameraModel(MessageWrapper wrapper)
//        {
//            var input = MessageDataHelper.FromMessageData<CameraModelName>(wrapper.messageData);
//            var res = logic.GetCameraCapacity(input.Name);
//            return ReturnMessageWrapper.CreateResult(true, wrapper, res, LocalizedString.OK);
//        }
//        public static ReturnMessageWrapper GetAllCameraModel(MessageWrapper wrapper)
//        {
//            var info = logic.GetAllCameraCapacity();
//            return ReturnMessageWrapper.CreateResult(true, wrapper, info, LocalizedString.OK);
//        }
//    }
//}
