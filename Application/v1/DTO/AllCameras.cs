using System.Collections.Generic;

namespace Monosoft.Service.CameraDB.V1.DTO
{
    public class AllCameras
    {
        public List<CameraInfo> cameraInfos { get; set; }
    }
}
