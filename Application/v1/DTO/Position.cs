﻿namespace Monosoft.Service.CameraDB.V1.DTO
{
    public class Position
    {
        public double x { get; set; }
        public double y { get; set; }
        public double angle { get; set; }
    }
}
