using System;

namespace Monosoft.Service.CameraDB.V1.DTO
{
    public class CameraRequest
    {
        public Guid id { get; set; }
    }
}
