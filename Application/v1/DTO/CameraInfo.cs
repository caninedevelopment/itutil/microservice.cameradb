using System;

namespace Monosoft.Service.CameraDB.V1.DTO
{
    public class CameraInfo
    {
        public Position position { get; set; }
        public Location location { get; set; }
        public string model { get; set; }
        public string name { get; set; }
        public string IP { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public Guid id { get; set; }
    }
}
