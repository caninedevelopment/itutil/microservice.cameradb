﻿using System.Collections.Generic;

namespace Monosoft.Service.CameraDB.V1.DTO
{

    public class CameraModelName
    {
        public string Name { get; set; }
    }

    public class CameraModelCapacity
    {
        public string name { get; set; }
        public List<Resolution> supportedResolutions { get; set; }
        public List<int> supportedCompression { get; set; }

        public List<RequestStreamUri.protocoltype> protocols { get; set; }
    }

    public class CameraModelCapacities
    {
        public List<CameraModelCapacity> CameraModelCApacities { get; set; }
    }

    public class Resolution
    {
        public int x { get; set; }
        public int y { get; set; }

        public List<int> supportedFPS { get; set; }
    }
}
