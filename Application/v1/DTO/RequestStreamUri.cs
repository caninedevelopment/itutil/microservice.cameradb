﻿using System;

namespace Monosoft.Service.CameraDB.V1.DTO
{
    public class RequestStreamUri
    {
        public enum protocoltype
        {
            http,
            https,
            rtp,
            rtsp
        }

        public Guid id { get; set; }
        public int resolutionX { get; set; }
        public int resolutionY { get; set; }
        public int fps { get; set; }
        public int compression { get; set; }
        public protocoltype protocol { get; set; }
    }
}
