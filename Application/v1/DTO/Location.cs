namespace Monosoft.Service.CameraDB.V1.DTO
{
    public class Location
    {
        public double latitude { get; set; }
        public double longitude { get; set; }
    }
}
