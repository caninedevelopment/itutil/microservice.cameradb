﻿using ITUtil.Common.Command;
using Monosoft.Service.CameraDB.v1;
using Monosoft.Service.CameraDB.V1.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.CameraDB.V1.Dto.CameraModel
{
    public class delete : DeleteCommand<CameraModelName>
    {

        public override string Description { get { return "Removes a model definition from database"; } }

        public override List<Claim> Claims { get { return new List<Claim>() { Namespace.IsAdmin }; } }

        public override void Execute(CameraModelName input)
        {
            if (input is null)
            {
                throw new ArgumentNullException(nameof(input));
            }

            Logic logic = new Logic();
            logic.DeleteCameraModel(input);
        }
    }
}
