﻿using ITUtil.Common.Command;
using Monosoft.Service.CameraDB.v1;
using Monosoft.Service.CameraDB.V1.DTO;
using System.Collections.Generic;

namespace Application.CameraDB.V1.Dto.CameraModel
{
    public class get : GetCommand<CameraModelName, CameraModelCapacity>
    {
        public override string Description { get { return "Gets the model definition"; } }

        public override List<Claim> Claims { get { return new List<Claim>() { Namespace.IsAdmin }; } }

        public override CameraModelCapacity Execute(CameraModelName input)
        {
            Logic logic = new Logic();
            return logic.GetCameraCapacity(input.Name);
        }
    }
}
