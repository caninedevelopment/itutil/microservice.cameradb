﻿using System.Collections.Generic;
using ITUtil.Common.Base;
using ITUtil.Common.Command;

namespace Application.CameraDB.V1.Dto.CameraModel
{
    public class Namespace : BaseNamespace
    {
        public static readonly Claim IsAdmin = new Claim()
        {
            key = "iTi.isAdmin",
            dataContext = Claim.DataContextEnum.organisationClaims,
            description = new LocalizedString[] { new LocalizedString("en", "User is administrator, i.e. is able to maintain the database") }
        };

        public Namespace() : base("CameraModel", new ProgramVersion("1.0.0.1"))
        {
            commands.AddRange(new List<ICommandBase>() {
                new getAll(),
                new delete(),
                new get(),
                new update()
            });
        }
    }
}