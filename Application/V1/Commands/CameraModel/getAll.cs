﻿using ITUtil.Common.Command;
using Monosoft.Service.CameraDB.v1;
using Monosoft.Service.CameraDB.V1.DTO;
using System;
using System.Collections.Generic;

namespace Application.CameraDB.V1.Dto.CameraModel
{
    public class getAll : GetCommand<object, CameraModelCapacities>
    {
        public override string Description { get { return "Gets the model definition"; } }

        public override List<Claim> Claims { get { return new List<Claim>() { Namespace.IsAdmin }; } }

        public override CameraModelCapacities Execute(object input)
        {
            if (input is null)
            {
                throw new ArgumentNullException(nameof(input));
            }

            Logic logic = new Logic();
            return new CameraModelCapacities() { CameraModelCApacities = logic.GetAllCameraCapacity() };
        }
    }
}
