﻿using ITUtil.Common.Command;
using Monosoft.Service.CameraDB.v1;
using Monosoft.Service.CameraDB.V1.DTO;
using System;
using System.Collections.Generic;

namespace Application.CameraDB.V1.Dto.CameraModel
{
    public class update : UpdateCommand<CameraModelCapacity>
    {
        public override string Description { get { return "Updates the model definition"; } }

        public override List<Claim> Claims { get { return new List<Claim>() { Namespace.IsAdmin }; } }

        public override void Execute(CameraModelCapacity input)
        {
            if (input is null)
            {
                throw new ArgumentNullException(nameof(input));
            }

            Logic logic = new Logic();
            logic.UpdateCameraModel(input);
        }
    }
}
