﻿using ITUtil.Common.Command;
using Monosoft.Service.CameraDB.v1;
using System;
using System.Collections.Generic;

namespace Application.CameraDB.V1.Dto.CameraDb
{
    public class scan : UpdateCommand<object>
    {
        public override string Description { get { return "Scans the network for camera and adds them to database"; } }

        public override List<Claim> Claims { get { return new List<Claim>() { Namespace.IsAdmin }; } }

        public override void Execute(object input)
        {
            if (input is null)
            {
                throw new ArgumentNullException(nameof(input));
            }

            Logic logic = new Logic();
            logic.ScanNetworkForCameras();
        }
    }
}
