﻿using ITUtil.Common.Command;
using Monosoft.Service.CameraDB.v1;
using Monosoft.Service.CameraDB.V1.DTO;
using System;
using System.Collections.Generic;

namespace Application.CameraDB.V1.Dto.CameraDb
{
    public class update : UpdateCommand<CameraInfo>
    {
        public override string Description { get { return "Updates a camera in the database"; } }

        public override List<Claim> Claims { get { return new List<Claim>() { Namespace.IsAdmin }; } }

        public override void Execute(CameraInfo input)
        {
            if (input is null)
            {
                throw new ArgumentNullException(nameof(input));
            }

            Logic logic = new Logic();
            logic.UpdateCamera(input);
        }
    }
}
