﻿using ITUtil.Common.Command;
using Monosoft.Service.CameraDB.v1;
using Monosoft.Service.CameraDB.V1.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.CameraDB.V1.Dto.CameraDb
{
    public class delete : DeleteCommand<CameraRequest>
    {
        public override string Description { get { return "Removes a cemera from database"; } }

        public override List<Claim> Claims { get { return new List<Claim>() { Namespace.IsAdmin }; } }

        public override void Execute(CameraRequest input)
        {
            if (input is null)
            {
                throw new ArgumentNullException(nameof(input));
            }

            Logic logic = new Logic();
            logic.DeleteCamera(input);
        }
    }
}
