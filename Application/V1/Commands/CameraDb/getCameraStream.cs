﻿using ITUtil.Common.Command;
using ITUtil.Common.RabbitMQ.DTO;
using Monosoft.Service.CameraDB.v1;
using Monosoft.Service.CameraDB.V1.DTO;
using System.Collections.Generic;

namespace Application.CameraDB.V1.Dto.CameraDb
{
    public class getCameraStream : GetCommand<RequestStreamUri, ResponseStreamUri>
    {

        public override string Description { get { return "Returns the stream uri for a camera"; } }

        public override List<Claim> Claims { get { return new List<Claim>() { Namespace.IsAdmin, Namespace.IsOperator }; } }

        public override ResponseStreamUri Execute(RequestStreamUri input)
        {
            Logic logic = new Logic();
            return logic.GetCameraStream(input);
        }
    }
}
