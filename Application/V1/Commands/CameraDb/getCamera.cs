﻿using ITUtil.Common.Command;
using Monosoft.Service.CameraDB.v1;
using Monosoft.Service.CameraDB.V1.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.CameraDB.V1.Dto.CameraDb
{
    public class getCamera : GetCommand<CameraRequest, CameraInfo>
    {
        public override string Description { get { return "Returns the information of the chosen camera"; } }

        public override List<Claim> Claims { get { return new List<Claim>() { Namespace.IsAdmin }; } }

        public override CameraInfo Execute(CameraRequest input)
        {
            Logic logic = new Logic();
            return logic.GetCamera(input);
        }
    }
}
