﻿using System.Collections.Generic;
using ITUtil.Common.Base;
using ITUtil.Common.Command;

namespace Application.CameraDB.V1.Dto.CameraDb
{
    public class Namespace : BaseNamespace
    {
        public static readonly Claim IsAdmin = new Claim()
        {
            key = "Monosoft.Service.CameraDb.isAdmin",
            dataContext = Claim.DataContextEnum.organisationClaims,
            description = new LocalizedString[] { new LocalizedString("en", "User is administrator") }
        };
        public static readonly Claim IsOperator = new Claim()
        {
            key = "Monosoft.Service.CameraDb.Operator",
            dataContext = Claim.DataContextEnum.organisationClaims,
            description = new LocalizedString[] { new LocalizedString("en", "User is an operator") }
        };

        public Namespace() : base("CameraDB", new ProgramVersion("1.0.0.1"))
        {
            commands.AddRange(new List<ICommandBase>(){
                new delete(),
                new getAllCamera(),
                new getCamera(),
                new getCameraStream(),
                new scan(),
                new update()
            });
        }
    }
}
