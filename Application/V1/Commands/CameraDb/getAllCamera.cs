﻿using ITUtil.Common.Command;
using Monosoft.Service.CameraDB.v1;
using Monosoft.Service.CameraDB.V1.DTO;
using System.Collections.Generic;

namespace Application.CameraDB.V1.Dto.CameraDb
{
    public class getAllCamera: GetCommand<object, AllCameras>
    {
        public override string Description { get { return "Returns a list of all cameras in the database"; } }

        public override List<Claim> Claims { get { return new List<Claim>() { Namespace.IsAdmin }; } }

        public override AllCameras Execute(object input)
        {
            Logic logic = new Logic();
            return logic.GetAllCameras();
        }
    }
}
