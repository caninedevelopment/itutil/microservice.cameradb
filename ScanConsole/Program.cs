﻿using Monosoft.Service.CameraDB.v1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Monosoft.Service.CameraDB
{
    class Program
    {
        private static void Main(string[] args)
        {
            Logic l = new Logic();
            l.ScanNetworkForCameras();
        }
    }
}