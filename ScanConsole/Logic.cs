namespace Monosoft.Service.CameraDB.v1
{
    using Ozeki.Media.IPCamera;
    using Ozeki.Media.IPCamera.Discovery;
    using System;
    using System.Collections.Generic;

    public class Logic
    {
        public Logic()
        {

        }

        private static readonly object scanPadlock = new object();
        List<DiscoveredDeviceInfo> _deviceList = new List<DiscoveredDeviceInfo>();
        private void IPCamera_DiscoveryCompleted(object sender, DiscoveryEventArgs e)
        {
            
            _deviceList.Add(e.Device);
        }

        public void ScanNetworkForCameras()
        {
            IPCameraFactory.DeviceDiscovered += IPCamera_DiscoveryCompleted;

            IPCameraFactory.DiscoverDevices();

            var starttime = DateTime.Now;
            while (DateTime.Now.Subtract(starttime).TotalSeconds < 10)
            {
                System.Threading.Thread.Sleep(1000);
            }
            IPCameraFactory.DeviceDiscovered -= IPCamera_DiscoveryCompleted;

            System.IO.File.WriteAllText("lastscan.json", Newtonsoft.Json.JsonConvert.SerializeObject(_deviceList));
        }
    }
}
